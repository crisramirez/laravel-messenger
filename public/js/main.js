var currentScrollY = 0;
var prevHeight = null;
var currentHeight = 0;
var maxScroll = 0;

$(document).ready(function () {

    // on load set scroll and init interval
    var messagesContainer = $('#messages');
    scrollToEnd(messagesContainer);

    // right now the auto refresh is disabled
    // let me show u what happens if we enable it and why we need a better solution
    if (false) {
        var t = setInterval(function () {
            $.ajax({
                url: window.location,
                success: function (response) {
                    // get new content
                    messagesContainer.html(response);

                    // get current scroll (vertical)
                    currentScrollY = messagesContainer.scrollTop() + messagesContainer.outerHeight();

                    // update prev and current height
                    prevHeight = currentHeight;
                    currentHeight = messagesContainer.get(0).scrollHeight;

                    // if heights are different -> new messages came in
                    if (prevHeight !== currentHeight) {
                        // if i am at the last known end -> scroll to new end
                        if (currentScrollY >= maxScroll) {
                            scrollToEnd(messagesContainer);
                        } else {
                            $("#scroll-to-end").removeClass('hide');
                        }
                    }
                }
            });
        }, 2000);
    }


    // submit form action
    $('#send-message').off('click').on('click', function () {
        // get url from form
        var url = $("#message-form").attr('action');
        // use jQuery.serialize to get all the fields in the form
        var data = $("#message-form").serialize();

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (response) {
                messagesContainer.html(response);
                scrollToEnd(messagesContainer);

                $('#message').val('');
            }
        })
    });

    // on scroll
    $('#messages').off('scroll').on('scroll', function () {
        // if scrolled all the way down, $("#scroll-to-end").addClass('hide');
    });

});

var scrollToEnd = function (ele) {

    /**
     * when I did ele.scrollHeight; it returned undefined
     * when i did ele.get(0).scrollHeight; it worked as expected
     * ele is a jQuery object
     * ele.get(0) is a DOM object
     * scrollHeight exists in a DOM object but not in a jQuery obejct
     *
     * before I made my changes cristian had ele[0].scrollHeight which is equivalent to ele.get(0).scrollHeight;
     */

        // gets the height of the scroll
    var height = ele.get(0).scrollHeight;

    // from the start make the scroll be at the buttom
    ele.scrollTop(height);

    maxScroll = height;

    $("#scroll-to-end").addClass('hide');
};
