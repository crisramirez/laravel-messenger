<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

// show all users
Route::get('/', 'UsersController@index')->name('users-index');

// show all my conversations
Route::get('/conversations', 'ConversationsController@index')->name('conversations-index');

// create a new conversation
Route::post('/conversations', 'ConversationsController@store')->name('conversations-store');

// show all messages in one conversation
Route::get('/conversations/{conversation}', 'ConversationsController@show')->name('conversations-show');

// delete conversation
Route::post('/conversations/{conversation}/delete', 'ConversationsController@destroy')->name('conversations-destroy');

// send a message
Route::post('/messages', 'MessagesController@store')->name('messages-store');

// delete a message
Route::post('/messages/{message}/delete', 'MessagesController@destroy')->name('messages-destroy');
