@foreach ($messages as $message)
    @if (!$message->didUserDelete(auth()->user()))
        <div class="add-bottom">
            @if($message->user_id == auth()->user()->id)
                <div id="message-{{ $message->id }}" class="message author-me pull-left">
                    @include('conversations.partials.message-body')
                </div>
                <div class="message-photo author-me pull-right">
                    <img src="/images/user-profile/placeholder.png">
                </div>
            @else
                <div id="message-{{ $message->id }}" class="message author-them pull-right">
                    @include('conversations.partials.message-body')
                </div>
                <div class="message-photo author-them pull-left">
                    <img src="/images/user-profile/placeholder.png">
                </div>
            @endif
        </div>
    @endif
@endforeach