<div class="message-action pull-right">
    <div class="dropdown">
        <button id="message-actions-{{ $message->id }}" class="btn btn-link dropdown-toggle"
                type="button" data-toggle="dropdown"></button>
        <div class="dropdown-menu">
            <form method="post"
                  action="{{ \Illuminate\Support\Facades\URL::route('messages-destroy',[$message->id]) }}">
                {{ csrf_field() }}
                <button type="submit" class="dropdown-item">
                    Delete
                </button>
            </form>
        </div>
    </div>
</div>

<div class="message-body">
    {{ nl2br($message->body) }}
</div>
<div class="message-date">
    {{ $message->created_at->format('g:i a') }}
</div>