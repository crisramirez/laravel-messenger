@extends('layouts.app')

@section('content')
        <div id="new-convo">
            <a href="{{ \Illuminate\Support\Facades\URL::route('users-index')  }}">
                <button>New <i class="fa fa-comments"></i></button>
            </a>
        </div>

        <ul class="list-group">
            @foreach($conversations as $conversation)
                <li class="list-group-item">
                    <div class="message-action pull-right">
                        <div id="dropdown">
                            <button id="message-actions-{{ $conversation->id }}" class="btn btn-link dropdown-toggle"
                                    type="button" data-toggle="dropdown"></button>
                            <div class="dropdown-menu">
                                <form method="post"
                                      action="{{ \Illuminate\Support\Facades\URL::route('conversations-destroy',[$conversation->id]) }}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="dropdown-item">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="message-photo pull-left"></div>

                    <div id="talking-with">
                        <a href="{{ \Illuminate\Support\Facades\URL::route('conversations-show',[$conversation->id])  }}">
                            @foreach($conversation->users(true)->get() as $user)
                                {{$user->name}}
                            @endforeach
                        </a>
                    </div>

                    @if ($conversation->messages()->count() > 0)
                        @if (strlen($conversation->messages()->latest()->first()->body) > 50)
                            <div class="last-message">{{substr($conversation->messages()->latest()->first()->body,0,50) . "..."}}</div>
                        @else
                            <div class="last-message">{{$conversation->messages()->latest()->first()->body}}</div>
                        @endif
                    @endif
                </li>
            @endforeach
        </ul>


    @if ($errors->any())
        {{ implode('', $errors->all(':message')) }}
    @endif
@endsection