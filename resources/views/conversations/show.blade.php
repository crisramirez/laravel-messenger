@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            {{ implode('', $errors->all(':message')) }}
        </div>
    @endif
                <div id="conversation-user">
                    @foreach($conversation->users(true)->get() as $user)
                        {{$user->name}}
                    @endforeach
                </div>
                <div id="messages" class="add-bottom">
                    @include('conversations.partials.messages')
                </div>

                <form id="message-form" method="post" action="{{ \route('messages-store') }}" onsubmit="return false;">

                    {{ csrf_field() }}
                    <input type="hidden" id="id" value="{{ $conversation->id }}" name="conversation_id">

                    <div class="form-group">
                        <textarea class="form-control no-resize" name="body" id="message" rows="3"></textarea>
                    </div>

                    <button id="send-message" type="button" class="btn btn-primary full-width block">Send message
                    </button>

                </form>

                <!--
                @todo when you scroll up and receive a message make you this comes out and that it looks nice
                to style it - have it show up at first all the time, once you are done styling it do the javascript
                to hide/show when it needs to
                -->
                <button class="btn btn-link hide" type="button" id="scroll-to-end">scroll down</button>

@endsection