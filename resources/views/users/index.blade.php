@extends('layouts.app')

@section('content')

    <form method="post" action="{{ \route('conversations-store') }}">

        {{ csrf_field() }}

        <fieldset class="form-group">
            <legend>Friends</legend>
            @foreach($users as $user)
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="user_id" id="{{ $user->id }}" value="{{ $user->id }}">
                    <label class="form-check-label">
                        {{ $user->name }}
                    </label>
                </div>
            @endforeach
        </fieldset>

        <button type="submit" class="btn btn-primary">Start conversation</button>
    </form>

    @if ($errors->any())
        {{ implode('', $errors->all(':message')) }}
    @endif

@endsection