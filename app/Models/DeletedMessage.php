<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DeletedMessage extends Model
{
    protected $guarded = [];
    protected $table = 'deleted_messages';

}