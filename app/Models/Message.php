<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    public function user()
    {
        // SELECT * FROM `users` WHERE `users`.`id` = {$this->user_id}
        return $this->belongsTo(User::class);
    }

    /**
     * Returns true if $user has NOT deleted $this message
     * Returns false otherwise
     *
     * A user deleted a message if:
     * There is a row in the database where user_id = $user->id and message_id = $this->id
     *
     * @param User $user
     * @return bool
     */
    public function didUserDelete(User $user) {
        return DeletedMessage::where([
            'user_id' => $user->id,
            'message_id' => $this->id
        ])->count() > 0;
    }
}