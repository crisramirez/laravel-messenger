<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    public function messages() {
        // SELECT * FROM `messages` WHERE `messages`.`conversation_id` = $this->id
        return $this->hasMany(Message::class);
    }

    public function users($excludeMe = false) {
        $query = $this->belongsToMany(User::class);
        if ($excludeMe && auth()->user() != null) {
            $query->where('conversation_user.user_id', '!=', auth()->user()->id);
        }
        return $query;
    }

}