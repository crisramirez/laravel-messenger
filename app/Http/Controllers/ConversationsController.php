<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\ConversationUser;
use App\Models\Message;
use App\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class ConversationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conversations = auth()->user()->conversations;

        return view('conversations.index', compact('conversations'));
    }

    /**
     * @param Conversation $conversation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Conversation $conversation)
    {

        $messages = $conversation->messages;
        $data = compact('messages', 'conversation');

        if (\request()->ajax()) {
            return view('conversations.partials.messages', $data);
        }

        return view('conversations.show', $data);


    }

    /**
     * Show the application dashboard.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $request->validate([
            'user_id' => 'required|numeric'
        ]);


        // getting all conversations for the logged in user
        $myConversations = Conversation::select(['conversations.id'])
            ->join('conversation_user', 'conversation_user.conversation_id', '=', 'conversations.id')
            ->where('conversation_user.user_id', '=', auth()->user()->id)->get()->toArray();

        // getting all the conversations for the user you're messaging
        $theirConversations = Conversation::select(['conversations.id'])
            ->join('conversation_user', 'conversation_user.conversation_id', '=', 'conversations.id')
            ->where('conversation_user.user_id', '=', $request->post('user_id'))->get()->toArray();

        $myConversations = array_flatten($myConversations);
        $theirConversations = array_flatten($theirConversations);

        // finds what both arrays have in common better
        $intersection = array_intersect($myConversations, $theirConversations);

        // if the arrays have nothing in common then save a new conversation
        if (count($intersection) == 0){
            $conversation = new Conversation;
            $conversation->save();

            // add users to my conversation
            ConversationUser::insert([
                [
                    'conversation_id' => $conversation->id,
                    'user_id' => $request->post('user_id'),
                ],
                [
                    'conversation_id' => $conversation->id,
                    'user_id' => auth()->user()->id,
                ],
            ]);
        } else {
            $conversation = Conversation::find(array_shift($intersection));
        }

        return redirect()->route('conversations-show', [
            'conversation' => $conversation->id
        ]);
    }

    /**
     * @param Conversation $conversation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Conversation $conversation)
    {
        $conversation->delete();
        return redirect()->route('conversations-index');
    }
}