<?php

namespace App\Http\Controllers;
;
use App\Models\Conversation;
use App\Models\DeletedMessage;
use App\Models\Message;

use Illuminate\Http\Request;

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required|min:1',
            'conversation_id' => 'required|numeric',
        ]);

        $message = new Message([
            'conversation_id' => $request->post('conversation_id'),
            'body' => $request->post('body'),
            'user_id' => auth()->user()->id
        ]);
        $message->save();

        // if ajax request, only return partial view
        if ($request->ajax()) {
            // get conversation using the id provided in the form data
            $conversation = Conversation::find($request->post('conversation_id'));
            // get all my messages
            $messages = $conversation->messages;
            // render partial view
            return view('conversations.partials.messages', compact('messages'));
        }

        return redirect()->route('conversations-show', [
            'conversation' => $request->post('conversation_id')
        ]);
    }

    public function destroy(Message $message){

        DeletedMessage::insert([
            [
                'user_id' => auth()->user()->id,
                'message_id' => $message->id
            ]
        ]);

        return redirect()->back();

    }
}
