<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id' , '!=', auth()->user()->id)->get();

        return view('users.index', compact('users'));
    }
}
