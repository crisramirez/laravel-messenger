<?php

namespace App;

use App\Models\Conversation;
use App\Models\ConversationUser;
use App\Models\Message;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function conversations()
    {

        // -- get me everything in conversations table
        // SELECT * FROM `conversations`
        // -- joins with conversation users table
        // INNER JOIN `conversation_user`
        // -- only join the conversations where conversation.id matches conversation_users.conversation_id
        // ON `conversation_user`.`conversation_id` = `conversation`.`id`
        // -- filter my results further to only return those who user id = $this->id
        // WHERE `users`.`id` = $this->id
        return $this->belongsToMany(Conversation::class);

    }
}
